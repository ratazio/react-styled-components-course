const Colors = {
  primary: '#006A5B',
  
  buttonTextDisabled: '#CCCCCC',
  buttonTextEnabled: '#FFF',
  buttonBgDisabled: 'transparent',
  buttonBgEnabled: "#1883FF",
  text: '#454545',
  placeholderText: '#c4c4c4',
};

export default Colors;
