const Fonts = {
    regular: 'HelveticaNeue',
    medium: 'HelveticaNeue',
    bold: 'HelveticaNeue',
    light: 'HelveticaNeue',
    fontSizePSmall: '16px',
  };
  
  export default Fonts;