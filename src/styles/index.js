import Fonts from './fonts';
import Colors from './colors';
import Shape from './shape'

export { Fonts, Colors, Shape };
