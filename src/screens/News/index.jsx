import React from 'react';
import MainTitle from '../../components/MainTitle';
import NewsContainer from "../../components/NewsContainer";
import * as Styled from './styles';

const NewsScreen = () => { 

    return (
        <Styled.DivNewsScreen>
        <MainTitle text="News" size="48px"/>
        <NewsContainer/>
        </Styled.DivNewsScreen>
    )
}

export default NewsScreen;