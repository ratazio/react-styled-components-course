import styled from "styled-components";

export const DivNewsScreen = styled.div`
    font-family: 'HelveticaNeue', sans-serif;
    background-color: #F3F3F3;
    padding: 20px;
`;