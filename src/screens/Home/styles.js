import styled from "styled-components";

export const Home = styled.div`
    padding: 20px;
    font-family: 'HelveticaNeue', sans-serif;
    background-color: #F3F3F3;
   
    .boards {
        margin-top: 70px;
        display: flex;
        justify-content: space-between;
        flex-wrap: wrap;

        > div:first-child, > div:nth-child(2) {
            flex: 0 1 35%;
        }

        > div:nth-child(3) {
            flex: 0 1 25%;
        }

        > div:nth-child(4) {
            flex: 0 1 72.5%;
        }

        > div:nth-child(5) {
            flex: 0 1 25%;
        }
    }
`;