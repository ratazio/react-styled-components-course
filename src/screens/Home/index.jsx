import React from 'react';
import BannerSlider from '../../components/BannerSlider';
import ProjectStatus from '../../components/ProjectStatus';
import { Home } from './styles';
import DashAndPendencies from './../../components/DashAndPendencies';
import TutorialsAndFAQ from '../../components/TutorialsAndFAQ';
import VideocallsAgenda from './../../components/VideocallsAgenda/index';
import Profile from '../../components/Profile';

const HomeScreen = () => { 

    return (
        <Home>
            
            {
             //   <BannerSlider />
            }
            <div className="boards">
            <ProjectStatus />
            <DashAndPendencies />
                <VideocallsAgenda />
                <TutorialsAndFAQ />
                <Profile />
                {
                    /*
                    
                
                */
                }
                
            </div>
        </Home>
    )
}

export default HomeScreen;