import HomeScreen from "./screens/Home"
import NewsScreen from "./screens/News";
import { useMediaQuery } from "./utils/mediaQuery"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  const [width] = useMediaQuery();

  return (
    
    <Router>    
        <Switch>
          <Route exact path="/" component={HomeScreen} />
          <Route path="/news" component={NewsScreen} />
       
        </Switch>
    </Router>

  );
}

export default App;
