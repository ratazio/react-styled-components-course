import React, { useEffect, useState } from "react";
import Button from "../Button";
import * as S from "./styles";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { strapiUrlsToDictionary } from "../../utils/cmsImageUrlPacker"
//strapiUrlsToDictionary

const BannerSlider = () => {
  const settings = {
    dots: true,
    infinite: true,
    arrows: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    customPaging: function (i) {
      return <a href="#i">{i + 1}</a>;
    },
  };
  const [banners, setBanners] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function getBanners() {
      fetch("https://bpool-cms.huiahlg.com.br/banners")
        .then((res) => res.json())
        .then((data) => {
          setLoading(false);
          setBanners(data);
        });
    }
    getBanners();
  }, []);

  function renderSlideImage(res, index) {
      let data = strapiUrlsToDictionary (res.image_highlight)
    return (
      <>
        <div key={index}>
          <S.SlideDiv>
            <S.SlideDivImage>
              <picture>
                <source
                  media="(min-width:1200px)"
                  srcset={data.large}
                />
                <source
                  media="(min-width:768px)"
                  srcset={data.medium}
                />
                <source
                  media="(min-width:465px)"
                  srcset={data.small}
                />
                <img src={data.fallback} alt="" />
              </picture>
            </S.SlideDivImage>
            {res.news ? (
              <S.SlideDivTexts>
                <S.SlideDivTextsTitle>{res.news.title}</S.SlideDivTextsTitle>
                <S.SlideDivTextsDescription>
                  {res.news.description}
                </S.SlideDivTextsDescription>
                <div>
                  <Button text="Leia mais" href={res.news.slug} />
                  <Button text="Ver todos" href="/news" />
                </div>
              </S.SlideDivTexts>
            ) : (
              <S.SlideDivTexts>
                <S.SlideDivTextsTitle>{res.title}</S.SlideDivTextsTitle>
                <S.SlideDivTextsDescription>
                  {res.description}
                </S.SlideDivTextsDescription>
                <Button text="Ver Mais" href={res.url} />
              </S.SlideDivTexts>
            )}
          </S.SlideDiv>
        </div>
      </>
    );
  }

  banners.forEach((item, index) => {
    console.log("Hightlight formats: ", item.image_highlight.formats);
  });

  return (
    <>
      {!loading && (
        <S.StyledSlider {...settings}>
          {banners.map((res, index) => renderSlideImage(res, index))}
        </S.StyledSlider>
      )}
    </>
  );
};

export default BannerSlider;
