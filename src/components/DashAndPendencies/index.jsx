import React, { useEffect, useState } from 'react';
import Button from '../Button';
import MainTitle from '../MainTitle';
import * as Styled from './styles';
import dashIcon from '../../assets/dash-icon.svg'
import pendencieIcon from '../../assets/pendencie-icon.svg'

const DashAndPendencies = () => { 
    const [loading, setLoading] = useState(true);
    const [onView, setOnView] = useState("dashboard")

    useEffect (() => {
        setLoading(false);
    }, [])

    return (
        <div>
        {!loading && (
            <>
            <MainTitle text="Central de Pendências e Dashboard"/>
            <Styled.DashPendencieBoard>
                <div>   
                        <div onClick={() =>  (setOnView("dashboard"))}>     
                            <Button 
                                text="Dashboard"
                                icon={dashIcon}
                                isDisabled={onView === "dashboard" ? false : true} 
                                padding="12px 35px"
                            />
                        </div>
                        <div onClick={() =>  (setOnView("pendencias"))}>    
                            <Button 
                                text="Pendências"
                                icon={pendencieIcon}  
                                isDisabled={onView === "pendencias" ? false : true} 
                                padding="12px 35px"
                            />
                        </div>
                </div>
                {onView === 'dashboard' 
                    ? (<div className="dashboard">
                            <h1>dashboard</h1>
                        </div>)
                    : ( <div className="pendencie">
                            <Styled.Pendencie color="red">
                                <p>Assinatura TEP Projeto Mobile - Bpool</p>
                                <div>
                                    <p>07/12/2021</p>
                                    <p>Em Andamento</p>
                                </div>
                            </Styled.Pendencie>
                            <Styled.Pendencie color="#1883FF">
                                <p>Assinatura TEP Projeto Mobile - Bpool</p>
                                <div>
                                    <p>07/12/2021</p>
                                    <p>Resolvido</p>
                                </div>
                            </Styled.Pendencie>
                            <Styled.Pendencie color="#1883FF">
                                <p>Assinatura TEP Projeto Mobile - Bpool</p>
                                <div>
                                    <p>07/12/2021</p>
                                    <p>Briefing e Cronograma</p>
                                </div>
                            </Styled.Pendencie>
                            
                            
                        </div>)
                }
                
               
                <Button text="Ir para Página"/>
            </Styled.DashPendencieBoard>
            </>
        )}
        </div>
    )
}

export default DashAndPendencies;