import styled from "styled-components";
import dateIcon from '../../assets/calendar-icon-gray.svg';
import clockIcon from '../../assets/clock-icon-gray.svg';
import profileIcon from '../../assets/profile-icon-gray.svg';

export const ContainerNews = styled.div`
   display: flex;
`;

export const DivNews = styled.div`
   background-color: #FFF;
   width: 33%;
   border-radius: 10px;
   overflow: hidden;

   img {
      width: 100%;
   }
`;

export const DivContent = styled.div`
   padding: 10px 30px;
`;

export const DivContentCategory = styled.p`
   font-size: 16px;
   line-height: 19px;
   color: #1883FF;
   font-weight: 600;
`;

export const DivContentTitle = styled.h1`
   font-size: 24px;
   line-height: 28px;
   font-weight: 600;
   color: #2F2F2F ;
`;

export const DivInfos = styled.div`
   display: flex;
   flex-direction: row;
   gap: 10px;
   font-size: 14px;
   line-height: 24px;
   color: #979797;
`;

export const DivContentDate = styled.p`
    &:before {
      content: '';
      width: 18px;
      height: 18px;
      background-image: url(${dateIcon});
      display: inline-flex;
      margin-right: 4px;
      bottom: -3px;
      position: relative;
    }
    &:after {
      content: '';
      width: 1px;
      height: 18px;
      display: inline-flex;
      background-color: #979797;
      margin-left: 15px;
      bottom: -2px;
      position: relative;
    }
`;

export const DivContentTime = styled.p`
    &:before {
      content: '';
      width: 18px;
      height: 18px;
      background-image: url(${clockIcon});
      display: inline-flex;
      margin-right: 4px;
      bottom: -3px;
      position: relative;
    }
    &:after {
      content: '';
      width: 1px;
      height: 18px;
      display: inline-flex;
      background-color: #979797;
      margin-left: 15px;
      bottom: -2px;
      position: relative;
    }
`;

export const DivContentAuthor = styled.p`
    &:before {
      content: '';
      width: 18px;
      height: 18px;
      background-image: url(${profileIcon});
      display: inline-flex;
      margin-right: 4px;
      bottom: -3px;
      position: relative;
    }
    &:after {
      content: '';
      width: 1px;
      height: 18px;
      display: inline-flex;
      background-color: #979797;
      margin-left: 15px;
      bottom: -2px;
      position: relative;
    }
`;

export const DivContentText = styled.p`
   font-size: 16px;
   line-height: 24px;
`;