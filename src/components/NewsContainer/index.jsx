import React, { useState, useEffect } from 'react';
import * as Styled from './styles';

const NewsContainer = () => {
    const [news, setNews] = useState([]);
    const [loading, setLoading] = useState(true);
    
    useEffect (() => {
        async function getNews() {
            fetch("https://bpool-cms.huiahlg.com.br/news")
            .then(res => res.json())
            .then((data) => {
                console.log(data)
                setLoading(false);
                setNews(data);
        })}
        getNews();
    }, [])

    return(
        <Styled.ContainerNews>
            {!loading &&
                news.map((res, index) => (
                    <Styled.DivNews key={index}>
                        <picture>
                            <img src={res.image.url} />
                        </picture>
                        <Styled.DivContent>
                            <Styled.DivContentCategory>{res.category.name}</Styled.DivContentCategory>
                            <Styled.DivContentTitle>{res.title}</Styled.DivContentTitle>
                            <Styled.DivInfos>
                                {/* <Styled.DivContentDate>{res.published_at}</Styled.DivContentDate> */}
                                <Styled.DivContentDate>01/02/21</Styled.DivContentDate>
                                <Styled.DivContentTime>3min</Styled.DivContentTime>
                                <Styled.DivContentAuthor>{res.author}</Styled.DivContentAuthor>
                            </Styled.DivInfos>
                                <Styled.DivContentText>{res.description}. <strong><a href={res.slug}>Ler mais</a></strong></Styled.DivContentText>
                        </Styled.DivContent>
                    </Styled.DivNews>
                ))
            }
        </Styled.ContainerNews>
    )
}

export default NewsContainer;