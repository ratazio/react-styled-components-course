import React from 'react';
import * as Styled from './styles';
import MainTitle from '../MainTitle/index';
import Button from '../Button';

const VideocallsAgenda = () => { 

    return (
       <div>
       <MainTitle text="Agenda de Videocalls"/>
       <Styled.AgendaBoard>
           <Styled.Call color="#1883FF">
               <div>
                   <p className="date">13 Dezembro 2021</p>
                   <p className="hour">14h00 - 15h00</p>
               </div>
               <div>
                   <p className="name">Fernanda Araújo</p>
                   <p className="client">Heineken</p>
               </div>
               <div>
                   <p className="status">Confirmada</p>
               </div>
           </Styled.Call>
           <Styled.Call color="red">
               <div>
                   <p className="date">14 Dezembro 2021</p>
                   <p className="hour">14h00 - 15h00</p>
               </div>
               <div>
                   <p className="name">Fernanda Araújo</p>
                   <p className="client">Heineken</p>
               </div>
               <div>
                   <p className="status">Aguardando confirmação</p>
               </div>
           </Styled.Call>
           <Button text="Ir para Videocalls" />
       </Styled.AgendaBoard>
       </div>
    );

}

export default VideocallsAgenda;