import React from 'react';
import { StyledTitle } from './styles';

const MainTitle = ({text, size}) => { 

    return (
       <StyledTitle size={size ? size : "24px"}>{text}</StyledTitle>
    );

}

export default MainTitle;