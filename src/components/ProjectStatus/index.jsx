import React, { useEffect } from 'react';
import Button from '../Button';
import MainTitle from '../MainTitle';
import { ProjectBoard } from './styles'

const ProjectStatus = () => { 

    useEffect (() => {
        
    }, [])

    return (
        <div>
        <MainTitle text="Últimos Status de Projetos"/>
        <ProjectBoard>
            <div>
                <div className="row">
                    <h2>Nome</h2>
                    <h2>Status</h2>
                </div>
                <div className="row">
                    <p>Campanha Unilever Fraldas</p>
                    <p>Aguardando matching</p>
                </div>
                <div className="row">
                    <p>Landing page Ford</p>
                    <p>PO</p>
                </div>
                <div className="row">
                    <p>Heineken Design Embalagem</p>
                    <p>Selecionar Parceiro</p>
                </div>
                <div className="row">
                    <p>JeJ	Planejamento Estratégico</p>
                    <p>Selecionar Parceiro</p>
                </div>
                <div className="row">
                    <p>Landing page Ford</p>
                    <p>PO</p>
                </div>
            </div>
            <Button text="Ir para Projetos" href="#"/>
        </ProjectBoard>
        </div>
    );

}

export default ProjectStatus;