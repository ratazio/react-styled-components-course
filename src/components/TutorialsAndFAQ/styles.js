import styled from "styled-components";

export const Board = styled.div`
    background-color: #FFF;
    border-radius: 10px;
    padding: 40px 30px 50px;
    text-align: center;

    > div:first-child {
        display: flex;
        justify-content: flex-start;
        gap: 12px;
    }   

    .tutorials {
        margin: 42px 0 30px;
        display: flex;
        flex-direction: row;
        gap: 30px;

        div {
            display: flex;
            flex-direction: column;
            text-align: left;

            div {
                padding: 30px 15px;
                span {
                    font-size: 16px;
                    color: #1883FF;
                    font-weight: 600;
                }
    
                p {
                    color: #2F2F2F;
                    font-weight: 600;
                    font-size: 20px;
                    line-height: 23px;
                }
            }
        }
    }

    .faq {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: space-between;
        margin: 42px 0 30px;

        div {
            flex: 0 1 30%;
            text-align: left;

            &:first-child, &:nth-child(2), &:nth-child(3) {
                border-bottom: 1px solid #d9d9d9;
                margin-bottom: 20px;
                padding-bottom: 20px;
            }
            h2{
                color: #2F2F2F;
                font-size: 20px;
                line-height: 23px;
                font-weight: 600;
            }
            p{
                font-size: 16px;
                line-height: 24px;
            }
        }
    }
`;