import React, { useState } from 'react';
import * as Styled from './styles';
import MainTitle from '../MainTitle/index';
import Button from '../Button';
import FAQIcon from '../../assets/faq-icon.svg';
import tutorialIcon from '../../assets/tutorials-icon.svg';
import imgTeste from '../../assets/faq-exemplo.png'

const TutorialsAndFAQ = () => { 
    const [onView, setOnView] = useState("faqs")

    return (
       <div>
       <MainTitle text="Tutoriais e FAQs"/>
       <Styled.Board>
            <div>
                <div onClick={() =>  setOnView("faqs")}>     
                    <Button  
                        text="FAQs"
                        icon={FAQIcon}
                        isDisabled={onView === "faqs" ? false : true} 
                        padding="12px 35px"
                    />
                </div>
                <div onClick={() => (setOnView("tutoriais"))}>    
                    <Button 
                        text="Tutoriais"
                        icon={tutorialIcon}  
                        isDisabled={onView === "tutoriais" ? false : true} 
                        padding="12px 35px"
                    />
                </div>
            </div>
            {onView === 'tutoriais' 
                ? (
                    <>
                    <div className="tutorials">
                        <div>
                            <img src={imgTeste}/>
                            <div>
                                <span>Vídeo</span>
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                            </div>
                        </div>
                        <div>
                            <img src={imgTeste}/>
                            <div>
                                <span>Vídeo</span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore </p>
                            </div>
                        </div>
                        <div>
                            <img src={imgTeste}/>
                            <div>
                                <span>Vídeo</span>
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                            </div>
                        </div>
                    </div>
                    <Button text="Ver Mais" />
                    </>
                ) : (
                    <>
                    <div className="faq">
                        <div>
                            <h2>Quam nibh eu vitae integer elit ac. Sem bibendum consectetur varius?</h2>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                        <div>
                            <h2>Quam nibh eu vitae integer elit ac. Sem bibendum consectetur varius?</h2>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                        <div>
                            <h2>Quam nibh eu vitae integer elit ac. Sem bibendum consectetur varius?</h2>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                        <div>
                            <h2>Quam nibh eu vitae integer elit ac. Sem bibendum consectetur varius?</h2>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                        <div>
                            <h2>Quam nibh eu vitae integer elit ac. Sem bibendum consectetur varius?</h2>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                        <div>
                            <h2>Quam nibh eu vitae integer elit ac. Sem bibendum consectetur varius?</h2>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                    <Button text="Ver Mais" />
                    </>
                )
            }
       </Styled.Board>
       </div>
    );

}

export default TutorialsAndFAQ;