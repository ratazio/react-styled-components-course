import React from 'react';
import * as Styled from './styles';
import MainTitle from '../MainTitle/index';
import imgTeste1 from '../../assets/profile-example.png';
import imgTeste2 from '../../assets/profile-example-2.png';
import Button from '../Button';

const Profile = () => {

    return (
        <div>
            <MainTitle text="Perfil" />
            <Styled.ProfileBoard>
                <div>
                    <div className="name">
                        <img src={imgTeste1} />
                        <div>
                            <h2>Unilever</h2>
                            <p>São Paulo</p>
                        </div>
                    </div>
                    <div className="responsible">
                        <h2>Responsável</h2>
                        <div>
                            <img src={imgTeste2} />
                            <div>
                                <h3>Fernando Tavares</h3>
                                <p>Executive Director</p>
                            </div>
                        </div>
                        <div>
                            <p><strong>Email:</strong> ftavares@unilever.com</p>
                            <p><strong>Cidade:</strong> São Paulo</p>
                        </div>
                    </div>
                </div>
                <div className="team">
                    <h2>Equipe Bpool</h2>
                    <div>
                        <img src={imgTeste2} />
                        <div>
                            <h3>Pedro Calazans</h3>
                            <p>Gerente Comercial</p>
                        </div>
                    </div>
                    <div>
                        <img src={imgTeste2} />
                        <div>
                            <h3>Danilo Moura</h3>
                            <p>Assistente Comercial</p>
                        </div>
                    </div>
                </div>
                <Button text="Ver Perfil" />
            </Styled.ProfileBoard>
        </div>
    );

}

export default Profile;