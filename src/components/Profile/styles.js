import styled from "styled-components";

export const ProfileBoard = styled.div`
    background-color: #FFF;
    border-radius: 10px;
    padding: 40px 30px 50px;
    text-align: center;

    img {
        width: 45px;
        border-radius: 50px;
    }
    h2 {
        font-size: 20px;
        font-weight: 600;
        line-height: 23px;
        margin: 0 0 3px;
        text-align: left;
    }
    h3 {
        font-size: 18px;
        font-weight: 600;
        line-height: 23px;
        margin: 0 0 3px;
        text-align: left;
    }
    p {
        margin: 0 0 3px;
        text-align: left;
    }

    > div {
        display: flex;
        flex-direction: column;

       
        .name {
            display: flex;
            flex-direction: row;
            align-items: center;
            gap: 15px;

            
            p{
                font-size: 14px;
                line-height: 20px;
            }

        }

        .responsible {
            padding: 30px 0;
            border-bottom: 1px solid #d9d9d9;

            > div:nth-child(2) {
                display: flex;
                flex-direction: row;
                align-items: center;
                gap: 15px;
                margin: 30px 0;

                p {
                   font-size: 14px;
                }

            }
            > div:nth-child(3) {
                p {
                   font-size: 16px;
                   margin: 0 0 10px;
                }
            }
        }
    }

    .team {
        margin: 30px 0 30px;

        h2 {
            margin-bottom: 30px;
        }
        > div {
            display: flex;
            flex-direction: row;
            align-items: center;
            margin-bottom: 20px;
            gap: 15px;

        }
    }
`;