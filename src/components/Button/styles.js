import styled from "styled-components";
import * as styleVars from "../../styles";

export const StyledButton = styled.a`
  cursor: pointer;
  border-radius: 28px;
  color: ${(props) =>
    props.enabled
      ? styleVars.Colors.buttonTextEnabled
      : styleVars.Colors.buttonTextDisabled};
  background-color: ${(props) => props.backgroundColor};
  font-size: ${styleVars.Fonts.fontSizePSmall};
  padding: ${(props) => props.padding};
  text-decoration: none;
  display: inline-flex;
  z-index: 999;
  border: 1px solid;
  border-color: ${(props) => props.borderColor};
  align-items: center;

  @media (max-width: 768px) {
    padding: 12px 35px;
  }

  img {
    width: 25px;
    margin-top: -3px;
    padding-right: 10px;
    filter: ${(props) => props.iconBrightness};
  }
`;
