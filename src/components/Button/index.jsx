import {StyledButton} from './styles';

const Button = ({text, href, icon, isDisabled, padding}) => { 
    return(
        <StyledButton 
            enabled={!isDisabled}
            backgroundColor={isDisabled ? "transparent" : "#1883FF"} 
            href={href}
            iconBrightness={isDisabled ? "brightness(85%)" : "brightness(100%)" }
            borderColor={isDisabled ? "#CCCCCC" : "transparent"}
            padding={padding ? padding : "14px 65px"}
        > 
            {icon && <img src={icon} alt={`ícone de ${text}`}/>}
            <span>{text}</span>
        </StyledButton> 
    )
};

export default Button;